'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StringLeaf = (_temp = _class = function (_Component) {
  (0, _inherits3.default)(StringLeaf, _Component);

  function StringLeaf(props) {
    (0, _classCallCheck3.default)(this, StringLeaf);

    var _this = (0, _possibleConstructorReturn3.default)(this, (StringLeaf.__proto__ || (0, _getPrototypeOf2.default)(StringLeaf)).call(this, props));

    _this.render = function () {
      return _react2.default.createElement(
        'div',
        { className: 'branch ' + _this.props.classNames.branch },
        _react2.default.createElement(
          'span',
          { className: 'prop ' + _this.props.classNames.prop },
          _this.props.root ? 'string' : _this.props.prop
        ),
        ':',
        _react2.default.createElement(
          'span',
          { className: 'leaf string-leaf ' + _this.props.classNames.leaf + ' ' + _this.props.classNames.stringLeaf },
          '"',
          _this.props.string,
          '"'
        )
      );
    };

    _this.state = {};
    return _this;
  }

  return StringLeaf;
}(_react.Component), _class.defaultProps = {
  prop: undefined,
  string: '',
  root: false,
  classNames: {
    branch: '',
    prop: '',
    leaf: '',
    stringLeaf: ''
  }
}, _temp);
exports.default = StringLeaf;