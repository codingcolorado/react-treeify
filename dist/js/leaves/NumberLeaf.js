'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NumberLeaf = (_temp = _class = function (_Component) {
  (0, _inherits3.default)(NumberLeaf, _Component);

  function NumberLeaf(props) {
    (0, _classCallCheck3.default)(this, NumberLeaf);

    var _this = (0, _possibleConstructorReturn3.default)(this, (NumberLeaf.__proto__ || (0, _getPrototypeOf2.default)(NumberLeaf)).call(this, props));

    _this.render = function () {
      return _react2.default.createElement(
        'div',
        { className: 'branch ' + _this.props.classNames.branch },
        _react2.default.createElement(
          'span',
          { className: 'prop ' + _this.props.classNames.prop },
          _this.props.root ? 'number' : _this.props.prop
        ),
        ':',
        _react2.default.createElement(
          'span',
          { className: 'leaf number-leaf ' + _this.props.classNames.leaf + ' ' + _this.props.classNames.numberLeaf },
          _this.props.number
        )
      );
    };

    _this.state = {};
    return _this;
  }

  return NumberLeaf;
}(_react.Component), _class.defaultProps = {
  prop: undefined,
  number: 0,
  root: false,
  classNames: {
    branch: '',
    prop: '',
    leaf: '',
    numberLeaf: ''
  }
}, _temp);
exports.default = NumberLeaf;