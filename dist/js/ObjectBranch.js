'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _leaves = require('./leaves');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ObjectBranch = (_temp = _class = function (_Component) {
  (0, _inherits3.default)(ObjectBranch, _Component);

  function ObjectBranch(props) {
    (0, _classCallCheck3.default)(this, ObjectBranch);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ObjectBranch.__proto__ || (0, _getPrototypeOf2.default)(ObjectBranch)).call(this, props));

    _this.toggleExpanded = function () {
      _this.setState({
        expanded: !_this.state.expanded
      });
    };

    _this.render = function () {
      var className = _this.props.classNames.leaf + ' ';
      // change this (not need line)

      switch ((0, _typeof3.default)(_this.props.value)) {
        case 'undefined':
          return _react2.default.createElement(_leaves.UndefinedLeaf, {
            root: _this.props.root,
            prop: _this.props.prop,
            classNames: {
              branch: _this.props.classNames.branch,
              prop: _this.props.classNames.prop,
              leaf: _this.props.classNames.leaf,
              undefinedLeaf: _this.props.classNames.undefinedLeaf
            } });
          break;
        case 'string':
          return _react2.default.createElement(_leaves.StringLeaf, {
            root: _this.props.root,
            prop: _this.props.prop,
            string: _this.props.value,
            classNames: {
              branch: _this.props.classNames.branch,
              prop: _this.props.classNames.prop,
              leaf: _this.props.classNames.leaf,
              stringLeaf: _this.props.classNames.stringLeaf
            } });
          break;
        case 'number':
          // handle NaN, infinity, & numbers
          if (isNaN(_this.props.value)) {
            return _react2.default.createElement(_leaves.NaNLeaf, {
              root: _this.props.root,
              prop: _this.props.prop,
              classNames: {
                branch: _this.props.classNames.branch,
                prop: _this.props.classNames.prop,
                leaf: _this.props.classNames.leaf,
                nanLeaf: _this.props.classNames.nanLeaf
              } });
          }

          if (_this.props.value == Infinity) {
            return _react2.default.createElement(_leaves.InfinityLeaf, {
              root: _this.props.root,
              prop: _this.props.prop,
              classNames: {
                branch: _this.props.classNames.branch,
                prop: _this.props.classNames.prop,
                leaf: _this.props.classNames.leaf,
                infinityLeaf: _this.props.classNames.infinityLeaf
              } });
          }

          return _react2.default.createElement(_leaves.NumberLeaf, {
            root: _this.props.root,
            prop: _this.props.prop,
            number: _this.props.value,
            classNames: {
              branch: _this.props.classNames.branch,
              prop: _this.props.classNames.prop,
              leaf: _this.props.classNames.leaf,
              numberLeaf: _this.props.classNames.numberLeaf
            } });
          break;
        case 'boolean':
          return _react2.default.createElement(_leaves.BooleanLeaf, {
            root: _this.props.root,
            prop: _this.props.prop,
            bool: _this.props.value,
            classNames: {
              branch: _this.props.classNames.branch,
              prop: _this.props.classNames.prop,
              leaf: _this.props.classNames.leaf,
              booleanLeaf: _this.props.classNames.booleanLeaf
            } });
          break;
        case 'function':
          return _react2.default.createElement(_leaves.FunctionLeaf, {
            root: _this.props.root,
            prop: _this.props.prop,
            func: _this.props.value,
            classNames: {
              branch: _this.props.classNames.branch,
              prop: _this.props.classNames.prop,
              leaf: _this.props.classNames.leaf,
              functionLeaf: _this.props.classNames.functionLeaf
            } });
          break;
        default:
          // handle objects, null, array, or Date
          if (!_this.props.value) {
            // null value
            return _react2.default.createElement(_leaves.NullLeaf, {
              root: _this.props.root,
              prop: _this.props.prop,
              classNames: {
                branch: _this.props.classNames.branch,
                prop: _this.props.classNames.prop,
                leaf: _this.props.classNames.leaf,
                nullLeaf: _this.props.classNames.nullLeaf
              } });
          }

          if (Object.prototype.toString.call(_this.props.value) === '[object Date]') {
            return _react2.default.createElement(_leaves.DateLeaf, {
              root: _this.props.root,
              prop: _this.props.prop,
              classNames: {
                branch: _this.props.classNames.branch,
                prop: _this.props.classNames.prop,
                leaf: _this.props.classNames.leaf,
                dateLeaf: _this.props.classNames.dateLeaf
              } });
          }

          // it's an array or an object
          var list = [];
          var isArray = false;
          if (Array.isArray(_this.props.value)) {
            isArray = true;

            _this.props.value.map(function (val, v) {
              list.push(_react2.default.createElement(ObjectBranch, {
                key: v,
                prop: v,
                value: val,
                expandedChar: _this.props.expandedChar,
                contractedChar: _this.props.contractedChar,
                classNames: _this.props.classNames
              }));
            });
          } else {
            (0, _keys2.default)(_this.props.value).forEach(function (key, k) {
              list.push(_react2.default.createElement(ObjectBranch, {
                key: k,
                prop: key,
                value: _this.props.value[key],
                expandedChar: _this.props.expandedChar,
                contractedChar: _this.props.contractedChar,
                classNames: _this.props.classNames
              }));
            });
          }

          var expandedChar = _this.state.expanded ? _this.props.expandedChar : _this.props.contractedChar;
          var rootLabel = isArray ? 'array' : 'object';

          if (list.length > 0) {
            var peak = null;
            if (!(_this.props.skipRoot && _this.props.root)) {
              peak = _react2.default.createElement(
                'div',
                { className: 'peak ' + _this.props.classNames.peak },
                _react2.default.createElement(
                  'a',
                  { className: 'expander ' + _this.props.classNames.expander, onClick: _this.toggleExpanded, href: 'javascript:void(0);' },
                  expandedChar,
                  ' \xA0',
                  _react2.default.createElement(
                    'span',
                    { className: 'prop ' + _this.props.classNames.prop },
                    _this.props.root ? rootLabel : _this.props.prop,
                    ' ',
                    isArray ? '[ ' + _this.props.value.length + ' ]' : '{ ' + (0, _keys2.default)(_this.props.value).length + ' }'
                  ),
                  ':'
                )
              );
            }

            return _react2.default.createElement(
              'div',
              { className: 'branch ' + _this.props.classNames.branch },
              peak,
              _react2.default.createElement(
                'div',
                { className: 'branches ' + _this.props.classNames.branches, style: { display: _this.state.expanded || _this.props.skipRoot && _this.props.root ? 'block' : 'none' } },
                list
              )
            );
          }

          return _react2.default.createElement(
            'div',
            { className: 'branch ' + _this.props.classNames.branch },
            _react2.default.createElement(
              'div',
              { className: 'peak ' + _this.props.classNames.peak },
              _react2.default.createElement(
                'span',
                { className: 'prop ' + _this.props.classNames.prop },
                _this.props.root ? rootLabel : _this.props.prop
              ),
              ': ',
              _react2.default.createElement(
                'span',
                { className: 'leaf ' + _this.props.classNames.leaf },
                isArray ? '[ ]' : '{ }'
              )
            )
          );

          break;
      }
    };

    _this.state = {
      expanded: false
    };
    return _this;
  }

  return ObjectBranch;
}(_react.Component), _class.defaultProps = {
  prop: undefined,
  value: undefined,
  root: false,
  skipRoot: false,
  expandedChar: '\u25BC',
  contractedChar: '\u25B6',
  classNames: {
    wrapper: '',
    branch: '',
    expander: '',
    branches: '',
    peak: '',
    prop: '',
    leaf: '',
    booleanLeaf: '',
    dateLeaf: '',
    functionLeaf: '',
    infinityLeaf: '',
    nanLeaf: '',
    nullLeaf: '',
    numberLeaf: '',
    stringLeaf: '',
    undefinedLeaf: ''
  }
}, _temp);
exports.default = ObjectBranch;