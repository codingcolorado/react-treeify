'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ObjectBranch = require('./ObjectBranch');

var _ObjectBranch2 = _interopRequireDefault(_ObjectBranch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ObjectTree = (_temp = _class = function (_Component) {
  (0, _inherits3.default)(ObjectTree, _Component);

  function ObjectTree(props) {
    (0, _classCallCheck3.default)(this, ObjectTree);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ObjectTree.__proto__ || (0, _getPrototypeOf2.default)(ObjectTree)).call(this, props));

    _this.defaultClassNames = {
      wrapper: '',
      branch: '',
      expander: '',
      branches: '',
      peak: '',
      prop: '',
      leaf: '',
      booleanLeaf: '',
      dateLeaf: '',
      functionLeaf: '',
      infinityLeaf: '',
      nanLeaf: '',
      nullLeaf: '',
      numberLeaf: '',
      stringLeaf: '',
      undefinedLeaf: ''
    };

    _this.render = function () {
      var classNames = (0, _assign2.default)({}, _this.defaultClassNames, _this.props.classNames || {});

      return _react2.default.createElement(
        'div',
        { className: 'react-treeify-wrapper ' + classNames.wrapper },
        _react2.default.createElement(_ObjectBranch2.default, {
          root: true,
          skipRoot: _this.props.skipRoot,
          value: _this.props.object,
          expandedChar: _this.props.expandedChar,
          contractedChar: _this.props.contractedChar,
          classNames: classNames
        })
      );
    };

    _this.state = {};
    return _this;
  }

  return ObjectTree;
}(_react.Component), _class.defaultProps = {
  object: undefined,
  skipRoot: false,
  expandedChar: '\u25BC',
  contractedChar: '\u25B6',
  classNames: {
    wrapper: '',
    branch: '',
    expander: '',
    branches: '',
    peak: '',
    prop: '',
    leaf: '',
    booleanLeaf: '',
    dateLeaf: '',
    functionLeaf: '',
    infinityLeaf: '',
    nanLeaf: '',
    nullLeaf: '',
    numberLeaf: '',
    stringLeaf: '',
    undefinedLeaf: ''
  }
}, _temp);
exports.default = ObjectTree;