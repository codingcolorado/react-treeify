'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ObjectTree = require('./ObjectTree');

var _ObjectTree2 = _interopRequireDefault(_ObjectTree);

var _ObjectBranch = require('./ObjectBranch');

var _ObjectBranch2 = _interopRequireDefault(_ObjectBranch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ObjectTree2.default;