# React Treeify
A customization-friendly JavaScript object tree inspector built for React.

## Getting Started
Start by installing the module:

`npm install --save react-treeify`

Import the module once it's installed:

`import ObjectTree from 'react-treeify'`

Send in an an object in the ObjectTree component:

`<ObjectTree object={{ some: 'obj', containing: 'some props' }} />`

### Import the SCSS or CSS Sheets Via JS

For now, the only way to actually style these things are to import the stylesheets (SASS/CSS):

`import 'react-treeify/src/styles/index.scss'`

or

`import 'react-treeify/dist/styles/index.css'`

### Import the SCSS Via SASS

`@import '~react-treeify/src/styles/index'`

### Import the Styles Via Hypertext Ref

You can grab the latest minified CSS file at https://bitbucket.org/codingcolorado/react-treeify/raw/master/dist/styles/index.min.css

## Styling Components

As of 1.0.6, you can style the different parts of the tree with your own class names. To do this, send in an object with the correct props and your desired class names. The component will add these class names to the rendered div, span, and anchor elements in the tree.

```
<ObjectTree object={someObj} classNames={{
  wrapper: 'my-wrapper-class',                  // div wrapping the entire component
  branch: 'my-branch-class and-second-class',   // div wrapping each branch in the tree
  expander: 'my-expander-class',                // anchor warpping the expander, for an array or object
  branches: 'my-branches-class',                // div wrapping object/array sub-branches
  peak: 'my-peak-class',                        // div, top-level branch
  prop: 'my-prop-class',                        // span with the prop name of an obj (or index of array)
  leaf: 'my-leaf-class',                        // span wrapping the value of a leaf on the branch (endpoint of a branch)
  booleanLeaf: 'my-boolean-leaf-class',         // span wrapping boolean endpoint of a branch
  dateLeaf: 'my-date-leaf-class',               // span wrapping date object' string rep at the endpoint of a branch
  functionLeaf: 'my-function-leaf-class',       // span wrapping function endpoint of a branch
  infinityLeaf: 'my-infinity-leaf-class',       // span wrapping infinity-value endpoint of a branch
  nanLeaf: 'my-nan-leaf-class',                 // span wrapping NaN endpoint of a branch
  nullLeaf: 'my-null-leaf-class',               // span wrapping null endpoint of a branch
  numberLeaf: 'my-number-leaf-class',           // span wrapping number endpoint of a branch
  stringLeaf: 'my-string-leaf-class',           // span wrapping string endpoint of a branch
  undefinedLeaf: 'my-undefined-leaf-class'      // span wrapping undefined endpoint of a branch
}} />
```

## Build in Progress
As of now, this project is far from complete. There are a few plans to add easy ways style certain things, possibly print out functions (if we can get it to work), and more.

If you have any suggestions for improvement, please [post an issue](https://bitbucket.org/codingcolorado/react-treeify).
