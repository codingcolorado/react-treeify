import React, { Component } from 'react';

import ObjectBranch from './ObjectBranch';

class ObjectTree extends Component {
  defaultClassNames = {
    wrapper: '',
    branch: '',
    expander: '',
    branches: '',
    peak: '',
    prop: '',
    leaf: '',
    booleanLeaf: '',
    dateLeaf: '',
    functionLeaf: '',
    infinityLeaf: '',
    nanLeaf: '',
    nullLeaf: '',
    numberLeaf: '',
    stringLeaf: '',
    undefinedLeaf: ''
  };

  constructor(props) {
    super(props);

    this.state = {
      
    };
  }

  static defaultProps = {
    object: undefined,
    skipRoot: false,
    expandedChar: '\u25bc',
    contractedChar: '\u25b6',
    classNames: {
      wrapper: '',
      branch: '',
      expander: '',
      branches: '',
      peak: '',
      prop: '',
      leaf: '',
      booleanLeaf: '',
      dateLeaf: '',
      functionLeaf: '',
      infinityLeaf: '',
      nanLeaf: '',
      nullLeaf: '',
      numberLeaf: '',
      stringLeaf: '',
      undefinedLeaf: ''
    }
  }

  render = () => {
    let classNames = Object.assign({}, this.defaultClassNames, this.props.classNames || {});

    return (
      <div className={'react-treeify-wrapper ' + classNames.wrapper}>
        <ObjectBranch 
          root={true} 
          skipRoot={this.props.skipRoot} 
          value={this.props.object} 
          expandedChar={this.props.expandedChar} 
          contractedChar={this.props.contractedChar} 
          classNames={classNames}
        />
      </div>
    );
  }
}

export default ObjectTree;