import React, { Component } from 'react';

class DateLeaf extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  static defaultProps = {
    prop: undefined,
    date: new Date(),
    root: false,
    iso: false,
    classNames: {
      branch: '',
      prop: '',
      leaf: '',
      dateLeaf: ''
    }
  }

  render = () => {
    return (
      <div className={'branch ' + this.props.classNames.branch}>
        <span className={'prop ' + this.props.classNames.prop}>
          {this.props.root ? 'date' : this.props.prop}
        </span>: 
        <span className={'leaf date-leaf ' + this.props.classNames.leaf + ' ' + this.props.classNames.dateLeaf}>
          {this.props.iso ? this.props.date.toISOString() : this.props.date.toString()}
        </span>
      </div>
    );
  }

}

export default DateLeaf;