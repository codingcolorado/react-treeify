import React, { Component } from 'react';

class NumberLeaf extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  static defaultProps = {
    prop: undefined,
    number: 0,
    root: false,
    classNames: {
      branch: '',
      prop: '',
      leaf: '',
      numberLeaf: ''
    }
  }

  render = () => {
    return (
      <div className={'branch ' + this.props.classNames.branch}>
        <span className={'prop ' + this.props.classNames.prop}>
          {this.props.root ? 'number' : this.props.prop}
        </span>: 
        <span className={'leaf number-leaf ' + this.props.classNames.leaf + ' ' + this.props.classNames.numberLeaf}>
          {this.props.number}
        </span>
      </div>
    );
  }

}

export default NumberLeaf;