import React, { Component } from 'react';

class FunctionLeaf extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  static defaultProps = {
    prop: undefined,
    func: () => { },
    root: false,
    classNames: {
      branch: '',
      prop: '',
      leaf: '',
      functionLeaf: ''
    }
  }

  render = () => {
    return (
      <div className={'branch ' + this.props.classNames.branch}>
        <span className={'prop ' + this.props.classNames.prop}>
          {this.props.root ? 'function' : this.props.prop}
        </span>: 
        <span className={'leaf function-leaf ' + this.props.classNames.leaf + ' ' + this.props.classNames.functionLeaf}>
          Function
        </span>
      </div>
    );
  }

}

export default FunctionLeaf;