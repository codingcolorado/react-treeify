import React, { Component } from 'react';

class BooleanLeaf extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  static defaultProps = {
    prop: undefined,
    bool: false,
    root: false,
    classNames: {
      branch: '',
      prop: '',
      leaf: '',
      numberLeaf: ''
    }
  }

  render = () => {
    return (
      <div className={'branch ' + this.props.classNames.branch}>
        <span className={'prop ' + this.props.classNames.prop}>
          {this.props.root ? 'bool' : this.props.prop}
        </span>: 
        <span className={'leaf boolean-leaf ' + this.props.classNames.leaf + ' ' + this.props.classNames.numberLeaf}>
          {this.props.bool ? 'true' : 'false'}
        </span>
      </div>
    );
  }

}

export default BooleanLeaf;