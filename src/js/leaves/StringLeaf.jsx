import React, { Component } from 'react';

class StringLeaf extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  static defaultProps = {
    prop: undefined,
    string: '',
    root: false,
    classNames: {
      branch: '',
      prop: '',
      leaf: '',
      stringLeaf: ''
    }
  }

  render = () => {
    return (
      <div className={'branch ' + this.props.classNames.branch}>
        <span className={'prop ' + this.props.classNames.prop}>
          {this.props.root ? 'string' : this.props.prop}
        </span>: 
        <span className={'leaf string-leaf ' + this.props.classNames.leaf + ' ' + this.props.classNames.stringLeaf}>
          "{this.props.string}"
        </span>
      </div>
    );
  }

}

export default StringLeaf;