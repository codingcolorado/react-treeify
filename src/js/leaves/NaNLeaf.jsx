import React, { Component } from 'react';

class NaNLeaf extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  static defaultProps = {
    prop: undefined,
    root: false,
    classNames: {
      branch: '',
      prop: '',
      leaf: '',
      nanLeaf: ''
    }
  }

  render = () => {
    return (
      <div className={'branch ' + this.props.classNames.branch}>
        <span className={'prop ' + this.props.classNames.prop}>
          {this.props.root ? 'NaN' : this.props.prop}
        </span>: 
        <span className={'leaf nan-leaf ' + this.props.classNames.leaf + ' ' + this.props.classNames.nanLeaf}>
          NaN
        </span>
      </div>
    );
  }

}

export default NaNLeaf;