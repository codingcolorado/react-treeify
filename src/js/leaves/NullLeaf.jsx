import React, { Component } from 'react';

class NullLeaf extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  static defaultProps = {
    prop: undefined,
    root: false,
    classNames: {
      branch: '',
      prop: '',
      leaf: '',
      nullLeaf: ''
    }
  }

  render = () => {
    return (
      <div className={'branch ' + this.props.classNames.branch}>
        <span className={'prop ' + this.props.classNames.prop}>
          {this.props.root ? 'object' : this.props.prop}
        </span>: 
        <span className={'leaf null-leaf ' + this.props.classNames.leaf + ' ' + this.props.classNames.nullLeaf}>
          null
        </span>
      </div>
    );
  }

}

export default NullLeaf;