import React, { Component } from 'react';

class InfinityLeaf extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  static defaultProps = {
    prop: undefined,
    root: false,
    classNames: {
      branch: '',
      prop: '',
      leaf: '',
      infinityLeaf: ''
    }
  }

  render = () => {
    return (
      <div className={'branch ' + this.props.classNames.branch}>
        <span className={'prop ' + this.props.classNames.prop}>
          {this.props.root ? 'number' : this.props.prop}
        </span>: 
        <span className={'leaf infinity-leaf ' + this.props.classNames.leaf + ' ' + this.props.classNames.infinityLeaf}>
          Infinity
        </span>
      </div>
    );
  }

}

export default InfinityLeaf;