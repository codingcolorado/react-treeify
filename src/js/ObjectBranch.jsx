import React, { Component } from 'react';

import {
  BooleanLeaf,
  DateLeaf,
  InfinityLeaf,
  FunctionLeaf,
  NaNLeaf,
  NullLeaf,
  NumberLeaf,
  StringLeaf,
  UndefinedLeaf
} from './leaves';

class ObjectBranch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: false
    };
  }

  static defaultProps = {
    prop: undefined,
    value: undefined,
    root: false,
    skipRoot: false,
    expandedChar: '\u25bc',
    contractedChar: '\u25b6',
    classNames: {
      wrapper: '',
      branch: '',
      expander: '',
      branches: '',
      peak: '',
      prop: '',
      leaf: '',
      booleanLeaf: '',
      dateLeaf: '',
      functionLeaf: '',
      infinityLeaf: '',
      nanLeaf: '',
      nullLeaf: '',
      numberLeaf: '',
      stringLeaf: '',
      undefinedLeaf: ''
    }
  }

  toggleExpanded = () => {
    this.setState({
      expanded: !this.state.expanded
    });
  }

  render = () => {
    let className = this.props.classNames.leaf + ' ';
    // change this (not need line)

    switch (typeof this.props.value) {
      case 'undefined':
        return (
          <UndefinedLeaf 
            root={this.props.root} 
            prop={this.props.prop}
            classNames={{
              branch: this.props.classNames.branch,
              prop: this.props.classNames.prop,
              leaf: this.props.classNames.leaf,
              undefinedLeaf: this.props.classNames.undefinedLeaf
            }} />
        );
        break;
      case 'string':
        return (
          <StringLeaf 
            root={this.props.root} 
            prop={this.props.prop} 
            string={this.props.value} 
            classNames={{
              branch: this.props.classNames.branch,
              prop: this.props.classNames.prop,
              leaf: this.props.classNames.leaf,
              stringLeaf: this.props.classNames.stringLeaf
            }} />
        );
        break;
      case 'number':
        // handle NaN, infinity, & numbers
        if (isNaN(this.props.value)) {
          return (
            <NaNLeaf 
              root={this.props.root} 
              prop={this.props.prop} 
              classNames={{
                branch: this.props.classNames.branch,
                prop: this.props.classNames.prop,
                leaf: this.props.classNames.leaf,
                nanLeaf: this.props.classNames.nanLeaf
              }} />
          );
        }

        if (this.props.value == Infinity) {
          return (
            <InfinityLeaf 
              root={this.props.root} 
              prop={this.props.prop} 
              classNames={{
                branch: this.props.classNames.branch,
                prop: this.props.classNames.prop,
                leaf: this.props.classNames.leaf,
                infinityLeaf: this.props.classNames.infinityLeaf
              }} />
          );
        }

        return (
          <NumberLeaf 
            root={this.props.root} 
            prop={this.props.prop} 
            number={this.props.value} 
            classNames={{
              branch: this.props.classNames.branch,
              prop: this.props.classNames.prop,
              leaf: this.props.classNames.leaf,
              numberLeaf: this.props.classNames.numberLeaf
            }} />
        );
        break;
      case 'boolean':
        return (
          <BooleanLeaf 
            root={this.props.root} 
            prop={this.props.prop} 
            bool={this.props.value} 
            classNames={{
              branch: this.props.classNames.branch,
              prop: this.props.classNames.prop,
              leaf: this.props.classNames.leaf,
              booleanLeaf: this.props.classNames.booleanLeaf
            }} />
        );
        break;
      case 'function':
        return (
            <FunctionLeaf 
              root={this.props.root} 
              prop={this.props.prop} 
              func={this.props.value}  
              classNames={{
                branch: this.props.classNames.branch,
                prop: this.props.classNames.prop,
                leaf: this.props.classNames.leaf,
                functionLeaf: this.props.classNames.functionLeaf
              }} />
          );
        break;
      default:
        // handle objects, null, array, or Date
        if (!this.props.value) {
          // null value
          return (
            <NullLeaf 
              root={this.props.root} 
              prop={this.props.prop} 
              classNames={{
                branch: this.props.classNames.branch,
                prop: this.props.classNames.prop,
                leaf: this.props.classNames.leaf,
                nullLeaf: this.props.classNames.nullLeaf
              }} />
          );
        }

        if (Object.prototype.toString.call(this.props.value) === '[object Date]') {
          return (
            <DateLeaf 
              root={this.props.root} 
              prop={this.props.prop}
              classNames={{
                branch: this.props.classNames.branch,
                prop: this.props.classNames.prop,
                leaf: this.props.classNames.leaf,
                dateLeaf: this.props.classNames.dateLeaf
              }} />
          );
        }

        // it's an array or an object
        let list = [];
        let isArray = false;
        if (Array.isArray(this.props.value)) {
          isArray = true;
          
          this.props.value.map((val, v) => {
            list.push((
              <ObjectBranch 
                key={v} 
                prop={v} 
                value={val} 
                expandedChar={this.props.expandedChar}
                contractedChar={this.props.contractedChar}
                classNames={this.props.classNames}
              />
            ));
          });
        } else {
          Object.keys(this.props.value).forEach((key, k) => {
            list.push((
                <ObjectBranch 
                  key={k} 
                  prop={key} 
                  value={this.props.value[key]} 
                  expandedChar={this.props.expandedChar}
                  contractedChar={this.props.contractedChar}
                  classNames={this.props.classNames}
                />
            ));
          });
        }

        let expandedChar = this.state.expanded ? this.props.expandedChar : this.props.contractedChar;
        let rootLabel = (isArray) ? 'array' : 'object';

        if (list.length > 0) {
          let peak = (null);
          if (!(this.props.skipRoot && this.props.root)) {
            peak = (
              <div className={'peak ' + this.props.classNames.peak}>
                <a className={'expander ' + this.props.classNames.expander} onClick={this.toggleExpanded} href="javascript:void(0);">
                  {expandedChar} &nbsp; 
                  <span className={'prop ' + this.props.classNames.prop}>{this.props.root ? rootLabel : this.props.prop} {isArray ? '[ ' + this.props.value.length + ' ]' : '{ ' + Object.keys(this.props.value).length + ' }'}</span>:
                </a>
              </div>
            );
          }
          
          return (
            <div className={'branch ' + this.props.classNames.branch}>
              {peak}

              <div className={'branches ' + this.props.classNames.branches} style={{display: ((this.state.expanded || (this.props.skipRoot && this.props.root)) ? 'block' : 'none')}}>
                {list}
              </div>
            </div>
          );
        }

        return (
          <div className={'branch ' + this.props.classNames.branch}>
            <div className={'peak ' + this.props.classNames.peak}>
              <span className={'prop ' + this.props.classNames.prop}>{this.props.root ? rootLabel : this.props.prop}</span>: <span className={'leaf ' + this.props.classNames.leaf}>{isArray ? '[ ]' : '{ }'}</span>
            </div>
          </div>
        )

        break;
    }
  }
}

export default ObjectBranch;